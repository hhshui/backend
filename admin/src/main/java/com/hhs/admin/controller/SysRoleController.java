package com.hhs.admin.controller;

import com.hhs.common.result.Re;
import com.hhs.module.sys.model.SysRole;
import com.hhs.admin.service.SysRoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author hhshui
 * @date 2024/4/11
 */
@Api(tags = "角色")
@RestController
@RequestMapping("/sys/role")
public class SysRoleController {

    @Resource
    SysRoleService sysRoleService;

    @ApiOperation(value = "新增")
    @PostMapping("/add")
    public Re<?> add(@RequestBody SysRole sysRole) {
        sysRoleService.add(sysRole);
        return Re.ok();
    }

    @ApiOperation(value = "修改")
    @PostMapping("/update")
    public Re<?> update(@RequestBody SysRole sysRole) {
        sysRoleService.update(sysRole);
        return Re.ok();
    }

    @ApiOperation(value = "列表")
    @PostMapping("/list")
    public Re<List<SysRole>> list() {
        return Re.data(sysRoleService.list(null));
    }
}
