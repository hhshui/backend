package com.hhs.admin.controller;

import com.hhs.module.msg.model.Notice;
import com.hhs.module.msg.service.NoticeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 *
 * @author hhshui
 * @date 2024/4/1
 *
 */
@Api(tags = "消息模块")
@RestController
@RequestMapping("/notice")
public class NoticeController {

    @Resource
    NoticeService noticeService;

    @ApiOperation(value = "消息列表")
    @GetMapping("/list")
    public List<Notice> list() {
        return noticeService.list(null);
    }
}
