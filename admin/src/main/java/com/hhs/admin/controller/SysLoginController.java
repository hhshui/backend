package com.hhs.admin.controller;

import com.hhs.admin.service.SysUserDetailsService;
import com.hhs.common.result.Re;
import com.hhs.module.sys.model.SysUser;
import com.hhs.admin.service.SysUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author hhshui
 * @date 2024/4/10
 */

@Api(tags = "登录")
@RestController
@RequestMapping("/sys")
public class SysLoginController {
    @Resource
    SysUserService sysUserService;

    @ApiOperation(value = "登录")
    @PostMapping("/login")
    public Re<String> login(@RequestBody SysUser vo) {
        String token = sysUserService.login(vo);
        if (null == token){
            return Re.fail("用户名或密码错误");
        }
        return Re.data(token);
    }

    @ApiOperation(value = "测试")
    @GetMapping("/test")
    public Re<String> test() {
        return Re.data("test");
    }
}
