package com.hhs.admin.service;

import com.hhs.module.sys.model.SysUser;
import org.springframework.security.core.userdetails.UserDetails;

public interface SysUserDetailsService {
    /**
     * 根据用户名获取用户对象（获取不到直接抛异常）
     */
    UserDetails loadUserByUsername(SysUser sysUser);
}
