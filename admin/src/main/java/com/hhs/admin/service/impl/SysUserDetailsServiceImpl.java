package com.hhs.admin.service.impl;

import com.hhs.admin.bo.SysUserDetail;
import com.hhs.admin.service.SysUserDetailsService;
import com.hhs.module.sys.model.SysUser;
import com.hhs.admin.service.SysUserService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author hhshui
 * @date 2024/4/11
 */
@Component
public class SysUserDetailsServiceImpl implements SysUserDetailsService {

    @Resource
    SysUserService sysUserService;

    @Override
    public UserDetails loadUserByUsername(SysUser sysUser) {
        SysUser one = sysUserService.getSysUserByName(sysUser.getUsername());
        if (null == one){
            throw new UsernameNotFoundException("用户名或密码错误");
        }
        return new SysUserDetail(one);
    }
}
