package com.hhs.admin.service.impl;

import cn.hutool.core.lang.UUID;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.hhs.security.custom.CustomMd5PasswordEncoder;
import com.hhs.admin.service.SysUserDetailsService;
import com.hhs.admin.service.SysUserService;
import com.hhs.common.base.BaseService;
import com.hhs.common.exception.BaseException;
import com.hhs.module.sys.mapper.SysUserMapper;
import com.hhs.module.sys.model.SysUser;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author hhshui
 * @date 2024/4/11
 */
@Service
public class SysUserServiceImpl extends BaseService<SysUserMapper, SysUser> implements SysUserService {

    @Resource
    SysUserDetailsService sysUserDetailsService;

    @Resource
    CustomMd5PasswordEncoder customMd5PasswordEncoder;

    @Override
    public SysUser getSysUserByName(String username) {
        return getOne(new LambdaQueryWrapper<SysUser>().eq(SysUser::getUsername, username));
    }

    @Override
    public String login(SysUser vo) {
        UserDetails userDetails = sysUserDetailsService.loadUserByUsername(vo);
        if(!customMd5PasswordEncoder.matches(vo.getPassword(),userDetails.getPassword())){
            throw new BaseException("密码不正确");
        }
        if (!userDetails.isEnabled()){
            throw new BaseException("用户被禁用");
        }
        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(authentication);
        return UUID.randomUUID().toString().replace("_","");
    }
}
