package com.hhs.admin.service.impl;

import com.hhs.common.base.BaseService;
import com.hhs.module.sys.mapper.SysRoleMapper;
import com.hhs.module.sys.model.SysRole;
import com.hhs.admin.service.SysRoleService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author hhshui
 * @date 2024/4/11
 */
@Service
public class SysRoleServiceImpl extends BaseService<SysRoleMapper, SysRole> implements SysRoleService  {
    @Override
    public void add(SysRole sysRole) {
        save(sysRole);
    }

    @Override
    public void update(SysRole sysRole) {
        baseMapper.updateById(sysRole);
    }

    @Override
    public List<SysRole> list(Object o) {
        return list(null);
    }
}
