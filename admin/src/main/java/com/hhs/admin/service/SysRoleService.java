package com.hhs.admin.service;

import com.hhs.module.sys.model.SysRole;

import java.util.List;

/**
 * @author hhshui
 * @date 2024/4/11
 */

public interface SysRoleService {
    List<SysRole> list(Object o);

    void add(SysRole sysRole);

    void update(SysRole sysRole);
}
