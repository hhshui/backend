package com.hhs.admin.service;

import com.hhs.module.sys.model.SysUser;

public interface SysUserService {

    SysUser getSysUserByName(String username);

    String login(SysUser vo);
}
