package com.hhs.mall.controller;

import com.hhs.module.msg.model.Notice;
import com.hhs.module.msg.service.NoticeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;

/**
 *
 * @author hhshui
 * @date 2024/4/1
 *
 */
@Api(tags = "消息模块")
@RestController
@RequestMapping("/notice")
public class NoticeController  {

    @Resource
    NoticeService noticeService;

    @ApiOperation(value = "列表")
    @GetMapping("/list")
    public List<Notice> list() {
        return noticeService.list(null);
    }
}
