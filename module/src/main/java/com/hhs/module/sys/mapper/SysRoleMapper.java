package com.hhs.module.sys.mapper;

import com.hhs.common.base.Mapper;
import com.hhs.module.sys.model.SysRole;

/**
 * @author admin
 */
public interface SysRoleMapper extends Mapper<SysRole> {
}
