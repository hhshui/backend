package com.hhs.module.sys.mapper;

import com.hhs.common.base.Mapper;
import com.hhs.module.sys.model.SysUser;

public interface SysUserMapper extends Mapper<SysUser> {
}
