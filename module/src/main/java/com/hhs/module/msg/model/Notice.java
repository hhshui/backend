package com.hhs.module.msg.model;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * @author hhshui
 * @date 2024/4/1
 */
@Data
@Getter
@Setter
@TableName("notice")
public class Notice {

    private String id;

    private String userId;

    private String context;

    private String state;

    private String type;

    private Integer creattime;

    private Integer updatetime;

}
