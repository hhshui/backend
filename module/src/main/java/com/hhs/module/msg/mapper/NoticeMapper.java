package com.hhs.module.msg.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hhs.module.msg.model.Notice;

public interface NoticeMapper extends BaseMapper<Notice> {
}
