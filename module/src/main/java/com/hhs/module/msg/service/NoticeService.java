package com.hhs.module.msg.service;

import com.hhs.common.base.BaseService;
import com.hhs.module.msg.mapper.NoticeMapper;
import com.hhs.module.msg.model.Notice;
import org.springframework.stereotype.Service;

/**
 * @author hhshui
 * @date 2024/4/1
 */
@Service
public class NoticeService extends BaseService<NoticeMapper, Notice> {
}
