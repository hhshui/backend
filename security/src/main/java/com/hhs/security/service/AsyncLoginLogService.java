package com.hhs.security.service;

public interface AsyncLoginLogService {

    /**
     * 记录登录信息
     *
     * @param username
     * @param status
     * @param ipaddr
     * @param message
     */
    void recordLoginLog(String username, Integer status, String ipaddr, String message);

}
