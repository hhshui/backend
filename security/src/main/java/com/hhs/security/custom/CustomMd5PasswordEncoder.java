package com.hhs.security.custom;

import cn.hutool.crypto.digest.DigestUtil;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

/**
 * @author hhshui
 * @date 2024/4/11
 * 密码处理
 */

@Component
public class CustomMd5PasswordEncoder implements PasswordEncoder {

    public String encode(CharSequence rawPassword) {
        return DigestUtil.bcrypt(rawPassword.toString());
    }

    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        return DigestUtil.bcryptCheck(rawPassword.toString(), encodedPassword);
    }
}
