package com.hhs.security.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

/**
 * @author hhshui
 * @date 2024/4/12
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig {
}
