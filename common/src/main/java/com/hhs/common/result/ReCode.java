package com.hhs.common.result;

/**
 * @author hhshui
 * @date 2024/4/9
 */

public enum ReCode {

    SUCCESS("0","成功"),
    FAILED("1","业务繁忙"),
    EXCEPTION("2", "系统繁忙,请稍后再试."),
    UNAUTHORIZED("401", "未登录或已过期.");
    private String code;
    private String message;

    private ReCode(String code, String message){
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
