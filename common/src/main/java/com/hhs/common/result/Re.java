package com.hhs.common.result;

/**
 * @author hhshui
 * @date 2024/4/9
 */

public class Re<T> {

    /**
     * 状态码
     */
    private String code;
    /**
     * 提示信息
     */
    private String msg;
    /**
     * 异常信息
     */
    private String ex;
    /**
     * 数据封装
     */
    private T data;

    public Re(){}

    public Re(String code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public Re(String code, String msg, String ex) {
        this.code = code;
        this.msg = msg;
        this.ex = ex;
    }
    public Re(String code, String msg, String ex, T data) {
        this.code = code;
        this.msg = msg;
        this.ex = ex;
        this.data = data;
    }

    public static <T> Re<T> ok() {
        return data(null);
    }

    public static <T> Re<T> data(T data) {
        return new Re<>(ReCode.SUCCESS.getCode(), ReCode.SUCCESS.getMessage(), data);
    }

    public static <T> Re<T> fail(String msg) {
        return fail(ReCode.FAILED.getCode(), msg);
    }

    public static <T> Re<T> fail(String code, String msg) {
        return fail(code, msg,null);
    }

    public static <T> Re<T> fail(ReCode reCode) {
        return fail(reCode.getCode(), reCode.getMessage());
    }

    public static <T> Re<T> fail(ReCode reCode,String ex) {
        return fail(reCode.getCode(), reCode.getMessage(), ex);
    }

    public static <T> Re<T> fail(String code, String msg,String ex) {
        return new Re<>(code, msg, ex);
    }

    public static <T> Re<T> error(String msg) {
        return fail(ReCode.EXCEPTION,msg);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getEx() {
        return ex;
    }

    public void setEx(String ex) {
        this.ex = ex;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
