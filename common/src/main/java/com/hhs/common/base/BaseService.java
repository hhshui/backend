package com.hhs.common.base;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author hhshui
 * @date 2024/4/1
 */

public class BaseService<M extends BaseMapper<T>,T> extends ServiceImpl<M,T> {

}
