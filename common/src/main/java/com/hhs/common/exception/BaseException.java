package com.hhs.common.exception;


import com.hhs.common.result.ReCode;
import lombok.Getter;


@Getter
public class BaseException extends RuntimeException {

    private String code;

    public BaseException(ReCode code) {
        this(code.getCode(), code.getMessage());
    }

    public BaseException(String message) {
        this(ReCode.EXCEPTION.getCode(), message);
    }

    public BaseException(Throwable cause) {
        this(ReCode.EXCEPTION.getCode(), cause);
    }

    public BaseException(String code, String message) {
        this(code, message, null);
    }

    public BaseException(String code, Throwable cause) {
        this(code, "", cause);
    }

    public BaseException(String code, String message, Throwable cause) {
        super(message, cause);
        this.code = code;
    }
}
